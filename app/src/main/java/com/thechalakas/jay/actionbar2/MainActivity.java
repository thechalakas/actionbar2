package com.thechalakas.jay.actionbar2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.action1:
                Log.i("MainActivity","Item 1 selected");
                // User chose the "Settings" item, show the app settings UI...
                return true;

            case R.id.action2:
                Log.i("MainActivity","Item 2 selected");
                // User chose the "Settings" item, show the app settings UI...
                return true;

            case R.id.action3:
                Log.i("MainActivity","Item 3 selected");
                // User chose the "Settings" item, show the app settings UI...
                return true;

            case R.id.action4:
                Log.i("MainActivity","Item 4 selected");
                // User chose the "Settings" item, show the app settings UI...
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
}
